Esse projeto foi idealizado para o trabalho final de Banco de Dados II do curso de Sistemas de Informação (UNIFESSPA - 2018), e demonstra a criação de uma loja de artigos para gamers. O objetivo principal do projeto é demonstrar uma integração funcional do programa com o banco de dados.
Para a criação do projeto foram utilizados:

Linguagem de Programação Back-End: PHP7, o PHP foi escolhido primeiramente pela integração relativamente simples com bancos de dados, e por possuir grande integração com HTML7 proporcionando assim mais facilidade na construção do Front-End.

Banco de Dados: MySQL pela eficiencia e simplicidade.

Arquitetura: MVC, por tornar o código mais organizado e assim aumentar a produtividade.

Bibliotecas Externas Utilizadas:
    FontAwesome v5.5.0<br>
    Bootstrap v4.1.3<br>
    DataTables v1.10.19<br>

O script do banco pode ser encontrado em /Core/migrations com nome migrations_v.1.5
