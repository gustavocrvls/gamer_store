

CREATE TABLE perfil(
	id_perfil int(11) AUTO_INCREMENT NOT NULL,
    descricao varchar(50),
    PRIMARY KEY(id_perfil)
);

create TABLE produto(
    id_produto int(11) not null AUTO_INCREMENT,
    nome varchar(40),
    quantidade_disponivel int(11),
    id_categoria int(11),
    PRIMARY KEY (id_produto),
    FOREIGN KEY (id_categoria) REFERENCES categoria(id_categoria)
);
