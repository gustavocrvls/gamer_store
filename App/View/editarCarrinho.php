<?php
include_once '../Controller/ControllerCompra.php';
include_once '../Model/Compra.php';

$controllerCompra = new ControllerCompra();

$controllerCompra->finalizarCompra();
$controllerCompra->removerProdutoEmCompra();
?>

<div class="container">
  <?php $controllerCompra->exibeCompraNaoFinalizada() ?>
</div>

<?php
$modelCompra = new Compra();
$result = $modelCompra->selectCompraUsuarioNaoFinalizada($_SESSION['id_usuario']);
if ($result) {
 echo '
<form action="index.php?action=editarCarrinho.php" method="post">
  <div class="text-center">
    <button class="btn btn-sm btn-dark" type="submit" name="finalizarCompra" onclick="return confirma(\'Deseja finalizar a compra?\')">Finalizar Compra</button>
  </div>
</form>
';
}
?>

<hr>
<div class="text-center">
  <a href="index.php?action=exibeProdutos.php" class="btn btn-sm btn-warning" value='Voltar'><i class="fa fa-angle-double-left"></i> Voltar</a>
</div>
