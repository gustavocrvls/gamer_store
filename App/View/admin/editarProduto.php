<?php
include_once '../Controller/ControllerProduto.php';
include_once '../Controller/ControllerCategoria.php';
include_once '../Model/Produto.php';

$controllerCategoria = new ControllerCategoria();
$controllerProduto = new ControllerProduto();
$modelProduto = new Produto();

$controllerProduto->editarProduto();

$produto = $modelProduto->selectProdutoById($_GET['id_produto']);
 ?>

<div class="container">
  <table style="width: 100%">
    <th style="width: 25%">
    </th>
    <th>
  <form class="form" action="index.php?action=admin/editarProduto.php&id_produto=<?php echo $produto['id_produto']?>" method="post">
    <div class="row">
      <span class="col-3">Nome: </span>
       <input type="text" class="form-control col-9" name="nome" value="<?php echo $produto['nome'];?>" placeholder="Nome"><br>
    </div>
    <br>
    <div class="row">
      <span class="col-3">Preço: </span>
      <input type="text" class="form-control col-9" name="preco" value="<?php echo $produto['valor'];?>" placeholder="Preço"><br>
    </div>
    <br>
    <div class="row">
      <span class="col-3">Quantidade:</span>
      <input type="text" class="form-control col-9" name="quantidade" value="<?php echo $produto['quantidade'];?>" placeholder="Quantidade"><br>
    </div>
    <br>
    <div class="row">
      <span class="col-3">Categoria:</span>
        <?php
        $controllerCategoria->getSelectOptionCategoriasEditar($produto['id_categoria']);
        ?>
    </div>
    <br>
    <div class="text-center">
      <input type="submit" class="btn btn-sm btn-dark" style="width: 150px" name="editar" value="Editar">
      <input type="hidden" name="id_produto" value="<?php echo $produto['id_produto']?>">
    </div>
  </form>
  <hr>
</th>
<th style="width: 25%">
</th>
</table>
</div>

<div class="text-center">
  <a href="index.php?action=admin/consultarProdutos.php" class="btn btn-sm btn-warning" value='Voltar'><i class="fas fa-angle-double-left"></i> Voltar</a>
</div>
