<?php

include_once '../Model/Compra.php';
include_once '../Model/CompraProduto.php';

class ControllerCompra
{
  public function finalizarCompra()
  {
    if (isset($_POST['finalizarCompra'])) {
      $modelCompra = new Compra();
      $result = $modelCompra->selectCompraUsuarioNaoFinalizada($_SESSION['id_usuario']);
      $modelCompra->finalizarCompra($result[0]['id_compra']);

      $modelCompra->insertCompra($_SESSION['id_usuario']);
      $novaCompra = $modelCompra->selectCompraUsuarioNaoFinalizada($_SESSION['id_usuario']);
      $_SESSION['id_compra'] = $novaCompra[0]['id_compra'];

      header('Location: index.php?action=exibeProdutos.php');
    }
  }

  public function inserirProdutoEmCompra()
  {
    if(isset($_POST['inserir']))
    {
      if($_SESSION && $_SESSION['id_usuario']){
        $modelCompra = new Compra();
        $modelCompraProduto = new CompraProduto();
        $id_produto = $_POST['id_produto'];
        $id_compra = $_SESSION['id_compra'];
        $quantidade = $_POST['quantidade'];
        $modelCompraProduto->insertCompraProduto($id_compra , $id_produto, $quantidade);
        header('Location: index.php?action=exibeProdutos.php');
      } else {
        header('Location: login.php');
      }
    }
  }

  public function exibeCompraNaoFinalizada()
  {
    $modelCompraProduto = new CompraProduto();
    $carrinho = $modelCompraProduto->selectCompraNaoFinalizadaByUsuario($_SESSION['id_usuario']);
    $totalGeral = 0;
    echo '<table class="table">';
    echo '<thead><th> </th><th>Produto</th><th>Preço</th><th>Quantidade</th><th>Total</th><th style="width:1%"> </th></thead><tbody>';
    foreach ($carrinho as $produto) {
      echo "<tr>";
      echo "<td style='width: 1%'>";
      switch ($produto['id_categoria']) {
        case 1:
        {
          echo '<i class="fas fa-mouse-pointer"></i>';
          break;
        }
        case 2:
        {
          echo '<i class="fas fa-tablet"></i>';
          break;
        }
        case 3:
        {
          echo '<i class="fas fa-keyboard"></i>';
          break;
        }
        case 4:
        {
          echo '<i class="fas fa-headphones-alt"></i>';
          break;
        }
        case 5:
        {
          echo '<i class="fas fa-headset"></i>';
          break;
        }
        case 6:
        {
          echo '<i class="fab fa-elementor"></i>';
          break;
        }
        case 7:
        {
          echo '<i class="fas fa-microchip"></i>';
          break;
        }
        case 8:
        {
          echo '<i class="fas fa-memory"></i>';
          break;
        }
        case 9:
          echo '<i class="fas fa-desktop"></i>';
          break;

        default:
          break;
      }
      echo "</td>";
      echo "<td>";
      echo $produto['nome'];
      echo "</td>";
      echo "<td>";
      echo 'R$ ' . number_format($produto['valor'], 2);
      echo "</td>";
      echo "<td>";
      echo $produto['qtd_comprada'];
      echo "</td>";
      echo "<td>";
      echo 'R$ ' . number_format($produto['qtd_comprada'] * $produto['valor'], 2);
      echo "</td>";
      echo '<td style="width: 1%">';
      echo '<form method="post" action="index.php?action=editarCarrinho.php">
      <button type="submit" class="btn btn-sm btn-outline-danger" name="remover"><i class="fas fa-times-circle"></i></button>
      <input type="hidden" name="id_compra_produto" value="'. $produto['id_compra_produto'] . '">
      </form>';
      echo "</td>";
      echo "</tr>";
      $totalGeral += $produto['qtd_comprada'] * $produto['valor'];
    }
    echo "<tr style='color: #000000'><td></td><th>Total Geral: </th><th></th><th></th><th>R$ $totalGeral.00</th><th></th></tr>";
    echo '</tbody></table>';
  }

  public function exibeCompraNaoFinalizadaConsulta()
  {
    $modelCompraProduto = new CompraProduto();
    $carrinho = $modelCompraProduto->selectCompraNaoFinalizadaByUsuario($_SESSION['id_usuario']);
    $totalGeral = 0;
    echo '<table class="table">';
    echo '<thead><th>Produto</th><th>Preço</th><th>Quantidade</th><th>Total</th></thead><tbody>';
    foreach ($carrinho as $produto) {
      echo "<tr>";
      echo "<td>";
      echo $produto['nome'];
      echo "</td>";
      echo "<td>";
      echo 'R$ ' . number_format($produto['valor'], 2);
      echo "</td>";
      echo "<td>";
      echo $produto['qtd_comprada'];
      echo "</td>";
      echo "<td>";
      echo 'R$ ' . number_format($produto['qtd_comprada'] * $produto['valor'], 2);
      echo "</td>";
      echo "</tr>";
      $totalGeral += $produto['qtd_comprada'] * $produto['valor'];
    }
    echo "<tr style='color: #000000'><th>Total Geral: </th><th></th><th></th><th>R$ $totalGeral.00</th></tr>";
    echo '</tbody></table>';
  }

  public function exibeAllCompra(){
    $modelCompra = new Compra();
    $compras = $modelCompra->selectAllCompra();
    $id_compra = 0;
    foreach ($compras as $compra) {
      if($id_compra != $compra['id_compra']) {
        if($id_compra != 0)
          echo "</tbody></table>";

        echo "<div class='row'>";
        echo "<h4  class='col-6' style='color: #336699'>{$compra['nome_usuario']}</h4>";
        echo "<h4 class='col-6 text-right' style='color: #336699'>" . date('d/m/Y', strtotime($compra['data_compra'])) . "</h4>";
        echo "</div>";
        echo "<table class='table'>";
        echo "<thead><th>Produto</th><th>Quantidade</th></thead>";
        echo "<tbody>";
        $id_compra = $compra['id_compra'];
      }
      echo "<tr>";
      echo "<td style='width: 80%'>{$compra['nome_produto']}</td>";
      echo "<td style='width: 20%'>{$compra['qtd_comprada']}</td>";
      echo "</tr>";
    }
    echo "</tbody></table>";
  }

  public function exibeAllCompraByUsuario($id_usuario){
    $modelCompra = new Compra();
    $compras = $modelCompra->selectAllCompraByUsuario($id_usuario);
    $id_compra = 0;

    if ($compras) {

    echo "<div class='row'><h3>{$compras[0]['nome_usuario']}</h3></div>";
    echo "<hr>";
    foreach ($compras as $compra) {
      if($id_compra != $compra['id_compra']) {
        if($id_compra != 0)
          echo "</tbody></table>";

        echo "<div class='row'>";
        echo "<h4 class='col-6' style='color: #336699'>" . date('d/m/Y', strtotime($compra['data_compra'])) . "</h4>";
        echo "</div>";
        echo "<table class='table'>";
        echo "<thead><th>Produto</th><th>Quantidade</th></thead>";
        echo "<tbody>";
        $id_compra = $compra['id_compra'];
      }
      echo "<tr>";
      echo "<td style='width: 80%'>{$compra['nome_produto']}</td>";
      echo "<td style='width: 20%'>{$compra['qtd_comprada']}</td>";
      echo "</tr>";
    }
    echo "</tbody></table>";
  } else {
    echo "<div class='alert alert-info'>Não há compras no seu perfil!</div>";
  }
  }

  public function removerProdutoEmCompra()
  {
    $modelCompraProduto = new CompraProduto();
    if (isset($_POST['remover'])){
      $modelCompraProduto->deleteCompraProdutoById($_POST['id_compra_produto']);
    }
  }
}
