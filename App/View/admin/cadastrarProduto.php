<?php
include_once '../Controller/ControllerProduto.php';
include_once '../Controller/ControllerCategoria.php';

$controllerCategoria = new ControllerCategoria();
$controllerProduto = new ControllerProduto();

$controllerProduto->cadastrarProduto();
 ?>

<div class="container">
  <table style="width: 100%">
    <th style="width: 25%">
    </th>
    <th>
  <form class="form" action="index.php?action=admin/cadastrarProduto.php" method="post">
    <div class="row">
      <span class="col-3">Nome: </span>
       <input type="text" class="form-control col-9" name="nome" placeholder="Nome"><br>
    </div>
    <br>
    <div class="row">
      <span class="col-3">Preço: </span>
      <input type="text" class="form-control col-9" name="preco" placeholder="Preço"><br>
    </div>
    <br>
    <div class="row">
      <span class="col-3">Quantidade:</span>
      <input type="text" class="form-control col-9" name="quantidade" placeholder="Quantidade"><br>
    </div>
    <br>
    <div class="row">
      <span class="col-3">Categoria:</span>
        <?php
        $controllerCategoria->getSelectOptionCategorias();
        ?>
    </div>
    <br>
    <div class="text-center">
      <input type="submit" class="btn btn-sm btn-dark" style="width: 150px" name="cadastrar" value="Cadastrar">
    </div>
  </form>
  <hr>
</th>
<th style="width: 25%">
</th>
</table>
</div>

<div class="text-center">
  <a href="index.php?action=admin/index_admin.php" class="btn btn-sm btn-warning" value='Voltar'><i class="fas fa-angle-double-left"></i> Voltar</a>
</div>
